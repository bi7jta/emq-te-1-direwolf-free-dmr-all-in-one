#!/bin/bash
#####################################################################################################################
mkdir /opt/YSFGateway2/
cp -r /opt/YSFGateway/* /opt/YSFGateway2/
cd /opt/YSFGateway2/
sudo chmod +x *
sudo sed -i "s/# YSF2DMRAddress=127.0.0.1/YSF2DMRAddress=127.0.0.1/g"  /opt/YSFGateway2/YSFGateway.ini
sudo sed -i "s/# YSF2DMRPort=42013/YSF2DMRPort=42013/g"  /opt/YSFGateway2/YSFGateway.ini
sudo sed -i "s/3230/3330/g"  /opt/YSFGateway2/YSFGateway.ini
sudo sed -i "s/Suffix=RPT/# Suffix=RPT/g"  /opt/YSFGateway2/YSFGateway.ini
sudo sed -i "s/# Suffix=ND/Suffix=ND/g"  /opt/YSFGateway2/YSFGateway.ini
sudo sed -i "s/4230/4330/g"  /opt/YSFGateway2/YSFGateway.ini
sudo sed -i "s/42013/42018/g"  /opt/YSFGateway2/YSFGateway.ini
sudo sed -i "s/42500/42400/g"  /opt/YSFGateway2/YSFGateway.ini
sudo sed -i "s/43001/44001/g"  /opt/YSFGateway2/YSFGateway.ini
sudo sed -i "s/# YSF2DMRAddress=127.0.0.1/YSF2DMRAddress=127.0.0.1/g"  /opt/YSFGateway2/YSFGateway.ini
sudo sed -i "s/# YSF2DMRPort=42013/YSF2DMRPort=42013/g"  /opt/YSFGateway2/YSFGateway.ini
sudo sed -i "s/6073/6075/g"  /opt/YSFGateway2/YSFGateway.ini
sudo sed -i "s/YSFGateway/YSFGateway2/g"  /opt/YSFGateway2/YSFGateway.ini
sudo sed -i "s/Iceberg/Panama/g"  /opt/YSFGateway2/YSFGateway.ini
sudo sed -i "s/DVSwitch/YSFGateway/g"  /opt/YSFGateway2/YSFGateway.ini
sudo sed -i "s/# Startup=Alabama-Link/Startup=EUROPELINK/g"  /opt/YSFGateway2/YSFGateway.ini
sudo sed -i "s/WiresXCommandPassthrough=0/WiresXCommandPassthrough=1/g"  /opt/YSFGateway2/YSFGateway.ini
#sudo sed -i "s/Enable=1/Enable=0/g"  /opt/YSFGateway2/YSFGateway.ini
sudo sed -i "s/var\/lib\/mmdvm/opt\/data-files/g"  /opt/YSFGateway2/YSFGateway.ini
sudo sed -i "s/N0CALL/HP3ICC/g"  /opt/YSFGateway2/YSFGateway.ini
sudo sed '1 i [Service]' -i /opt/YSFGateway2/YSFGateway.ini
sudo sed '2 i ServiceStart=0' -i /opt/YSFGateway2/YSFGateway.ini
sudo sed '2 G ' -i /opt/YSFGateway2/YSFGateway.ini
###################
cat > /lib/systemd/system/ysfgw.service  <<- "EOF"
[Unit]
Description=YSF Gateway 2 Service
# Description=Place this file in /lib/systemd/system
# Description=N4IRS 01/17/2020
After=mmdvmh.service
#After=netcheck.service
#Requires=netcheck.service

[Service]
#User=root
#Type=simple
#Restart=always
#RestartSec=3
#StandardOutput=null
WorkingDirectory=/opt/YSFGateway2
ExecStart=/opt/YSFGateway2/YSFGateway /opt/YSFGateway2/YSFGateway.ini
Restart=on-failure

[Install]
WantedBy=multi-user.target


EOF




##############################################################################################################

systemctl daemon-reload
