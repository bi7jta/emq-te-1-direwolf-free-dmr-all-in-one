#!/bin/bash
if [ "$(cat /proc/cpuinfo | grep 'Raspberry')" != "" ]; then
cd /opt
wget https://raw.githubusercontent.com/balena-os/wifi-connect/master/scripts/raspbian-install.sh
sed -i "s/CONFIRMATION=.*/CONFIRMATION=false/" raspbian-install.sh
chmod +x raspbian-install.sh
./raspbian-install.sh
rm raspbian-install.sh

#auto ap
sudo cat > /usr/local/bin/autoap.sh <<- "EOF"
#!/bin/bash

# Tiempo de espera entre los chequeos en segundos
TIEMPO_ESPERA=30

# Lista de servicios para verificar y detener si no hay conexión a Internet
SERVICIOS=("http.server-dw.service" "http.server-mmdvmh.service" "http.server-dvs.service" "http.server-ysf.service" "hbmon.service" "hbmon2.service" "hbmon-js.service" "http.server-fdmr.service" "http.server-fdmr2.service" "daprs-board.service")

# Funcion para verificar el estado de Internet
verificar_internet() {
  ping -c 5 noip.com > /dev/null 2>&1
  return $?
}

# Funcion para detener un servicio si está habilitado
detener_servicio() {
  servicio="$1"
  if sudo systemctl is-enabled "$servicio" >/dev/null 2>&1; then
    sudo systemctl stop "$servicio"
  fi
}

# Bucle infinito para monitorear
while true; do
  # Verificar el estado de la interfaz wlan0
  if iw dev wlan0 link | grep -q "Not connected"; then
    # La interfaz wlan0 no esta conectada, continuar con la verificacion de Internet
    if verificar_internet; then
      # Accion silenciosa
      :
    else
      # Primera prueba de ping negativa
      sleep $TIEMPO_ESPERA

      # Segunda prueba de ping
      if verificar_internet; then
        # Segunda prueba de ping exitosa, no hace nada
        :
      else
        # Internet no esta disponible, realiza acciones necesarias
        for servicio in "${SERVICIOS[@]}"; do
          detener_servicio "$servicio"
        done
        wifi-connect
      fi
    fi
  else
    # La interfaz wlan0 esta conectada, esperar antes de la proxima verificacion
    sleep $TIEMPO_ESPERA
  fi

  sleep $TIEMPO_ESPERA
done


EOF

sudo cat > /usr/local/bin/apoff.sh <<- "EOF"
#!/bin/bash

# Lista de servicios a verificar y reiniciar si están habilitados
SERVICIOS=("http.server-dw.service" "http.server-mmdvmh.service" "http.server-dvs.service" "http.server-ysf.service" "hbmon.service" "hbmon2.service" "hbmon-js.service" "http.server-fdmr.service" "http.server-fdmr2.service" "daprs-board.service")

while true; do
    # Verifica si wifi-connect está corriendo como proceso
    if pgrep -x "wifi-connect" >/dev/null; then
        # Realiza una prueba de ping a noip.com
        if ping -c 1 noip.com >/dev/null 2>&1; then
            # Si la respuesta de ping es positiva, reinicia autoap.service
            sudo systemctl restart autoap.service

            # Verifica y reinicia los servicios habilitados de la lista
            for servicio in "${SERVICIOS[@]}"; do
                if sudo systemctl is-enabled "$servicio" >/dev/null 2>&1; then
                    sudo systemctl restart "$servicio"
                fi
            done
        fi
    fi

    # Espera 3 segundos antes de realizar la siguiente iteración
    sleep 3
done


EOF
sudo chmod +x /usr/local/bin/apoff.sh
sudo chmod +x /usr/local/bin/autoap.sh
#
sudo cat > /lib/systemd/system/apoff.service  <<- "EOF"
[Unit]
Description=Rebooter Service1
After=network.target

[Service]
ExecStart=/usr/local/bin/apoff.sh
Restart=on-failure

[Install]
WantedBy=default.target

EOF
#
sudo cat > /lib/systemd/system/autoap.service  <<- "EOF"
[Unit]
Description=AutoAP
After=network-pre.target
Wants=network-pre.target

[Service]
#ExecStartPre=/bin/sleep 60
ExecStart=/usr/local/bin/autoap.sh
Restart=on-failure

[Install]
WantedBy=default.target

EOF
#
systemctl daemon-reload
systemctl enable autoap.service
systemctl enable apoff.service
#systemctl stop apoff.service autoap.service
#systemctl start autoap.service apoff.service

fi
