#!/bin/bash
###########################################################################################################################
#                                                       noip
###########################################################################################################################

sudo mkdir /opt/noip
cd /opt/noip/
wget https://www.noip.com/client/linux/noip-duc-linux.tar.gz
tar vzxf noip-duc-linux.tar.gz
cd noip-2.1.9-1
sudo make
