#!/bin/bash
cd /opt/
if [ "$(uname -m | grep '64')" != "" ]; then
 # echo "ARCH: 64-bit"
wget -qO gotty.tar.gz https://github.com/yudai/gotty/releases/latest/download/gotty_linux_amd64.tar.gz
else
#  echo "ARCH: 32-bit"
wget -qO gotty.tar.gz https://github.com/yudai/gotty/releases/latest/download/gotty_linux_arm.tar.gz
fi

sudo tar xf gotty.tar.gz -C /usr/local/bin
sudo chmod a+x /usr/local/bin/gotty
rm gotty.tar.gz

#
sudo cat > /lib/systemd/system/gotty.service <<- "EOF"
[Unit]
Description=GoTTY
After=multi-user.target

[Service]
User=root
Environment=TERM=xterm-256color
#ExecStartPre=/bin/sleep 40
ExecStart=gotty -p "8022" -w -c "pi:Panama507" menu
Restart=on-failure

[Install]
WantedBy=multi-user.target

EOF

systemctl daemon-reload
sudo systemctl enable gotty.service

