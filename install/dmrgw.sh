#!/bin/bash
##################################################################################################################################
#                                                        DMRGateway
#################################################################################################################################
cd /opt/	
git clone https://github.com/g4klx/DMRGateway.git
cd DMRGateway
sudo make clean
sudo make
sudo make install

###################

sudo cat > /lib/systemd/system/dmrgw.service  <<- "EOF"
[Unit]
Description=DMRGateway Service
After=syslog.target network.target
After=mmdvmh.service
 
[Service]
#User=root
#Type=simple
#Restart=always
#RestartSec=3
#StandardOutput=null
WorkingDirectory=/opt/DMRGateway
#ExecStartPre=/bin/sleep 15
ExecStart=/usr/local/bin/DMRGateway /opt/DMRGateway/DMRGateway.ini
Restart=on-failure
#ExecStop=/usr/bin/screen -S DMRGateway -X quit


[Install]
WantedBy=multi-user.target
 
EOF
####################
sudo cat > /opt/DMRGateway/DMRGateway.ini  <<- "EOF"
[Service]
ServiceStart=0 

[General]
Timeout=10
# RFTimeout=10
# NetTimeout=7
RptAddress=127.0.0.1
RptPort=62034
LocalAddress=127.0.0.1
LocalPort=62033
RuleTrace=0
Daemon=0
Debug=0

[Log]
# Logging levels, 0=No logging
DisplayLevel=1
FileLevel=1
FilePath=.
FileRoot=DMRGateway
FileRotate=1

[Voice]
Enabled=1
Language=es_ES
Directory=./Audio

[Info]
Latitude=0.0
Longitude=0.0
Height=0
Location=DMR Gateway
Description=Multi-Mode DMRGateway
URL=https://gitlab.com/hp3icc/emq-TE1

[XLX Network]
#DMRID 7 DIGIT ONLY
Id=1234567 
Enabled=0
File=/opt/data-files/XLXHosts.txt
Port=62030
Password=passw0rd
ReloadTime=60
# Local=3351
Slot=2
TG=6
Base=64000
Startup=744
Relink=10
Debug=0
#Allow user linking control using Private Calls
UserControl=1
#Override default module for startup reflector
Module=A

# BrandMeister
[DMR Network 1]
#Id=000000000
Enabled=0
Name=BrandMeister
Address=3021.master.brandmeister.network
Port=62031
TGRewrite0=2,1000001,2,1,999999
PCRewrite=2,1000001,2,1,999999
#PassAllTG=1
#PassAllPC=1
Password=passw0rd
Location=1
Debug=0

# FreeDMR
[DMR Network 2]
#Id=000000000
Enabled=0
Name=FreeDMR
TGRewrite0=2,2000001,2,1,999999
PCRewrite=2,2000001,2,1,999999
#PassAllTG=2
#PassAllPC=2
# Puede activar linea de options de selfcare y colocar su propia contraseña o 
# utilizar linea de options con opciones de tg estaticos 
Options="PASS=abc123"
#Options="TS2=91;DIAL=0;VOICE=0;LANG=es_ES;SINGLE=1;TIMER=10;"
Address=freedmr-hp.ddns.net
Password=passw0rd
Port=62031
Location=0
Debug=0


# TGIF Network
[DMR Network 3]
#Id=000000000
Enabled=0
Name=TGIF_Network
TGRewrite0=2,3000001,2,1,999999
PCRewrite=2,3000001,2,1,999999
#Options=
Address=tgif.network
Password=passw0rd
Port=62031
Location=0
Debug=0

# DMR Central
[DMR Network 4]
#Id=000000000
Enabled=0
Name=DMR Central
Address=dmr.pa7lim.nl
Port=55555
TGRewrite0=2,4000001,2,1,999999
PCRewrite=2,4000001,2,1,999999
#Options=
Password=passw0rd
Location=0
Debug=0

# FreeStar Network
[DMR Network 5]
#Id=000000000
Enabled=0
Name=FreeStar
Address=es.freestar.network
Port=62031
TGRewrite0=2,5000001,2,1,999999
PCRewrite=2,5000001,2,1,999999
Options="TS2=91;DIAL=0;VOICE=0;LANG=es_ES;SINGLE=1;TIMER=10;"
Password=passw0rd
Location=0
Debug=0


[GPSD]
Enable=0
Address=127.0.0.1
Port=2947

[APRS]
Enable=0
Address=127.0.0.1
Port=8673
Description=APRS Description
Suffix=3

[Dynamic TG Control]
Enabled=1
Port=3769

[Remote Control]
Enable=0
Address=127.0.0.1
Port=7643


EOF
#########
