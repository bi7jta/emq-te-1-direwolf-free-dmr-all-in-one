#!/bin/bash
if [ -d "/opt/NXDN2DMR/" ]
then
  rm -r /opt/NXDN2DMR/
fi
if [ -d "/var/log/nxdn2dmr" ]
then
  echo "found dir"
else 
  mkdir /var/log/nxdn2dmr
fi
cp -r /opt/MMDVM_CM/NXDN2DMR/ /opt/
sudo chmod +x /opt/NXDN2DMR/*
cd /opt/NXDN2DMR/
sudo make
###########################
sudo cat > /lib/systemd/system/nxdn2dmr.service  <<- "EOF"
[Unit]
Description=NXDN2DMR Service
After=syslog.target network.target

[Service]
#User=root
#Type=simple
#Restart=always
#RestartSec=3
#StandardOutput=null
WorkingDirectory=/opt/NXDN2DMR
#ExecStartPre=/bin/sleep 15
ExecStart=/opt/NXDN2DMR/NXDN2DMR /opt/NXDN2DMR/NXDN2DMR.ini
Restart=on-failure

[Install]
WantedBy=multi-user.target

EOF
###########################
sudo cat > /opt/NXDN2DMR/NXDN2DMR.ini  <<- "EOF"
[Info]
RXFrequency=0
TXFrequency=0
Power=1
Latitude=0.0
Longitude=0.0
Height=0
Location=NXDN Bridge
Description=Multi-Mode Repeater
URL=www.google.co.uk

[NXDN Network]
Callsign=NOCALL
TG=9
DstAddress=100.100.100.100
DstPort=41400
LocalAddress=127.0.0.1
#LocalPort=42022
DefaultID=7336
Daemon=0

[DMR Network]
Id=123456789
#XLXFile=XLXHosts.txt
#XLXReflector=950
#XLXModule=D
StartupDstId=7144
# For TG call: StartupPC=0
StartupPC=0
Address=127.0.0.1
Port=62031
Jitter=500
# Local=62032
Password=passw0rd
Options=
#TS2=9;DIAL=0;VOICE=0;LANG=es_ES;SINGLE=1;TIMER=0;
#XLX:4001
Debug=0

[DMR Id Lookup]
File=/opt/data-files/DMRIds.dat
Time=24

[NXDN Id Lookup]
File=/opt/data-files/NXDN.csv
Time=24

[Log]
# Logging levels, 0=No logging
DisplayLevel=1
FileLevel=0
FilePath=.
FileRoot=NXDN2DMR


EOF

#######################################################################################################################
systemctl daemon-reload
