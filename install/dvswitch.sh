#!/bin/bash
#############################################################################################################################
#                                                           dvswitch
#############################################################################################################################
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/Easy-DVSwitch/-/raw/main/install.sh)"
sudo cat > /usr/local/dvs/dvs <<- "EOF"
#!/bin/bash

#===================================
SCRIPT_VERSION="Menu Script v.1.61"
SCRIPT_AUTHOR="HL5KY"
SCRIPT_DATE="11/06/2020"
#===================================

source /var/lib/dvswitch/dvs/var.txt

if [ "$1" != "" ]; then
    case $1 in
        -v|-V|--version) echo "dvs "$SCRIPT_VERSION; exit 0 ;;
        -a|-A|--author) echo "dvs "$SCRIPT_AUTHOR; exit 0 ;;
        -d|-D|--date) echo "dvs "$SCRIPT_DATE; exit 0 ;;
          *) echo "Unknown parameter passed: $1"; exit 1 ;;
    esac
fi

#--------------------------------------------------------------------

clear

# After upgrading, if [there is dvsm.basic] -> meaning setting is Advanced Macro Configuration
if [ -e ${AB}dvsm.basic ]; then
#    if there is not character "Advanced" in dvsm.macro -> updated & upgraded and dvsm.macro is brand new
        if [[ -z `grep "Advanced" ${AB}dvsm.macro` ]]; then
                sudo \cp -f ${adv}dvsm.macro ${AB}dvsm.macro
        fi
fi


if [ -e /var/lib/dvswitch/dvs/var.old ]; then
clear
sudo \mv -f /var/lib/dvswitch/dvs/var.old /var/lib/dvswitch/dvs/var.txt
source /var/lib/dvswitch/dvs/var.txt
fi


if [ ! -e ${lan}language.txt ]; then
clear
sudo \cp -f ${lan}english.txt ${lan}language.txt
source /var/lib/dvswitch/dvs/var.txt
fi


if [ "$startup_lan" != "73" ]; then

clear

update_var startup_lan 73

	if (whiptail --title " Change Language Settings " --yesno "\

           Do you want to change Language settings now ?


           You can do it later. The menu is under <Tools>
	" 12 70); then
	${DVS}language.sh; exit 0
	fi
fi


#--------------------------------------------------------------------

OPTION=$(whiptail --title " $T010 " --menu "\
                                           $SCRIPT_VERSION\n
\n
" 14 110 6 \
"01 $T011     " "$T012" \
"02 $T013     " "$T014" \
"03 $T015     " "$T016" \
"04 $T023     " "$T024" \
"05 $T017     " "$T018" \
"06 $T019     " "$T020" 3>&1 1>&2 2>&3)

if [ $? != 0 ]; then
clear;
exit 0
fi

case $OPTION in
01\ *)sudo ${DVS}init_config.sh ;;
02\ *)sudo ${DVS}adv_config_menu.sh ;;
03\ *)sudo ${DVS}tools_menu.sh ;;
04\ *)bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/service/dv-list-tg.sh)" ;;
05\ *)sudo ${DVS}credits.sh ;;
06\ *)exit 0
esac

#exit 0

EOF
###
cd /tmp/
wget https://gitlab.com/hp3icc/DVSwitch-Mobile-TG-List/-/raw/main/lang.sh
sudo chmod +x lang.sh
sh lang.sh
###
sed -i "s/default_dmr_server=.*/default_dmr_server=FreeDMR/g"  /var/lib/dvswitch/dvs/var.txt
sed -i "s/other1_name=.*/other1_name=FreeDMR/g"  /var/lib/dvswitch/dvs/var.txt
sed -i "s/other1_address=.*/other1_address=freedmr-hp.ddns.net/g"  /var/lib/dvswitch/dvs/var.txt
sed -i "s/other1_password=.*/other1_password=passw0rd/g"  /var/lib/dvswitch/dvs/var.txt
sed -i "s/other1_port=.*/other1_port=62031/g"  /var/lib/dvswitch/dvs/var.txt
sed -i "s/other2_name=.*/other2_name=DMR-Central/g"  /var/lib/dvswitch/dvs/var.txt
sed -i "s/other2_address=.*/other2_address=dmr.pa7lim.nl/g"  /var/lib/dvswitch/dvs/var.txt
sed -i "s/other2_password=.*/other2_password=passw0rd/g"  /var/lib/dvswitch/dvs/var.txt
sed -i "s/other2_port=.*/other2_port=55555/g"  /var/lib/dvswitch/dvs/var.txt
sed -i "s/42000/42500/g" /opt/YSFGateway/YSFGateway.ini
sed -i "s/42001/43001/g" /opt/YSFGateway/YSFGateway.ini
sed -i "s/RptPort=3200/RptPort=3230/g" /opt/YSFGateway/YSFGateway.ini
sed -i "s/LocalPort=4200/LocalPort=4230/g" /opt/YSFGateway/YSFGateway.ini
if [ -d "/var/www/dvs" ]
then
  rm -r /var/www/dvs
fi
mkdir /var/www/dvs
mv /var/www/html/* /var/www/dvs/ &&
sed -i "s/'UTC'/'Africa\/Lagos'/"  /var/www/dvs/include/*.php
sed -i "s/www\/html/www\/dvs/g" /var/www/dvs/include/* &&
#sed -i "s/usrp.duckdns.org\/user.csv/datafiles.ddns.net:8888\/user.csv/g" /var/www/dvs/include/* &&
sed -i "s/Dashboard based on Pi-Star Dashboard, © Andy Taylor.*/Dashboard based on Pi-Star Dashboard, © Andy Taylor (MW0MWZ) and adapted to DVSwitch by SP2ONG<br> <a title=\"Raspbian Proyect by HP3ICC © <?php \$cdate=date(\"Y\"); if (\$cdate > \"2018\") {\$cdate=\"2018-\".date(\"Y\");} echo \$cdate; ?>\" target=\"_blank\" href=https:\/\/gitlab.com\/hp3icc\/emq-TE1\/>Proyect: emq-TE1ws+<\/a><\/span><\/center>/" /var/www/dvs/index.php &&
sed -i "s/<meta name=\"Description\".*/<meta name=\"Description\" content=\"Dashboard based on Pi-Star Dashboard, © Andy Taylor (MW0MWZ) and adapted to DVSwitch by SP2ONG\" \/>/" /var/www/dvs/index.php &&
cp /var/www/dvs/index.php /opt/index-dvs.txt
sed -i "s/www\/html/www\/dvs/g" /usr/local/sbin/update-config.sh
sed -i "s/www\/html/www\/dvs/g" /var/lib/dpkg/info/dvswitch*
sed -i "s/Language=en_US/Language=es_ES/g" /opt/NXDNGateway/NXDNGateway.ini
sed -i "s/Language=en_US/Language=es_ES/g" /opt/P25Gateway/P25Gateway.ini 
sed -i "s/8080/9009/g" /var/www/dvs/include/*.*
if [ ! -d "/var/www/dvs/qrz_photo" ]
then
  sudo mkdir /var/www/dvs/qrz_photo
fi
sudo chmod o+w /var/www/dvs/qrz_photo
if grep -q "8080" /lib/systemd/system/webproxy.service; then
    systemctl stop webproxy.service
    sed -i "s/8080/9009/g" /opt/Web_Proxy/proxy.js
    sed -i "s/8080/9009/g" /lib/systemd/system/webproxy.service
    systemctl daemon-reload
fi
if ! sudo systemctl status webproxy.service |grep "service; enabled;" >/dev/null 2>&1
   then 
   sudo systemctl enable webproxy.service
fi
if ! systemctl status webproxy.service |grep "Active: active" >/dev/null 2>&1
  then sudo systemctl restart webproxy.service

fi
#
sudo cat > /opt/MMDVM_Bridge/MMDVM_Bridge.ini  <<- "EOF"
[General]
Callsign=N0CALL
Id=1234567
Timeout=300
Duplex=0

[Info]
RXFrequency=147000000
TXFrequency=147000000
Power=1
Latitude=8.5211
Longitude=-80.3598
Height=0
Location=DVSwitch Server
Description=MMDVM DVSwitch
URL=https://groups.io/g/DVSwitch

[Log]
# Logging levels, 0=No logging, 1=Debug, 2=Message, 3=Info, 4=Warning, 5=Error, 6=Fatal
DisplayLevel=1
FileLevel=1
FilePath=/var/log/mmdvm
FileRoot=MMDVM_Bridge

[DMR Id Lookup]
File=/var/lib/mmdvm/DMRIds.dat
Time=24

[NXDN Id Lookup]
File=/var/lib/mmdvm/NXDN.csv
Time=24

[Modem]
Port=/dev/null
RSSIMappingFile=/dev/null
Trace=0
Debug=0

[D-Star]
Enable=0
Module=B

[DMR]
Enable=0
ColorCode=1
EmbeddedLCOnly=1
DumpTAData=0

[System Fusion]
Enable=0

[P25]
Enable=0
NAC=293

[NXDN]
Enable=0
RAN=1
Id=12345

[D-Star Network]
Enable=0
GatewayAddress=127.0.0.1
GatewayPort=20010
LocalPort=20011
Debug=0

[DMR Network]
Enable=0
Address=hblink.dvswitch.org
Port=62031
Jitter=500
Local=62032
Password=passw0rd
# for DMR+ see https://github.com/DVSwitch/MMDVM_Bridge/blob/master/DOC/DMRplus_startup_options.md
# for XLX the syntax is: Options=XLX:4009
# No active linea de Option para TG estaticos, si utiliza BM,TGIF,DMR-Central
# Puede activar linea de option de selfcare FDMR-Mon y colocar su propia contraseña o 
# utilizar linea de options con opciones de tg estaticos
#Options=PASS=abc123
#Options=TS2=714,7144;DIAL=0;VOICE=0;LANG=es_ES;SINGLE=0;TIMER=10;
Slot1=0
Slot2=1
Debug=0

[System Fusion Network]
Enable=0
LocalAddress=0
LocalPort=3230
GatewayAddress=127.0.0.1
GatewayPort=4230
Debug=0

[P25 Network]
Enable=0
GatewayAddress=127.0.0.1
GatewayPort=42020
LocalPort=32010
Debug=0

[NXDN Network]
Enable=0
#LocalAddress=127.0.0.1
Debug=0
LocalPort=14021
GatewayAddress=127.0.0.1
GatewayPort=14020
EOF
####
sudo systemctl daemon-reload
sudo systemctl disable lighttpd.service
sudo systemctl stop lighttpd.service
sudo systemctl stop md380-emu.service
sudo systemctl stop analog_bridge.service
sudo systemctl stop mmdvm_bridge.service
sudo systemctl stop nxdngateway.service
sudo systemctl stop p25gateway.service
sudo systemctl stop ysfgateway.service
sudo systemctl stop quantar_bridge.service
sudo systemctl stop ircddbgatewayd.service
sudo systemctl stop p25parrot.service
sudo systemctl stop ysfparrot.service
sudo systemctl stop nxdnparrot.service
sudo systemctl stop monit.service
sudo systemctl stop webproxy.service
sudo systemctl disable md380-emu.service
sudo systemctl disable analog_bridge.service
sudo systemctl disable mmdvm_bridge.service
sudo systemctl disable nxdngateway.service
sudo systemctl disable p25gateway.service
sudo systemctl disable ysfgateway.service
sudo systemctl disable quantar_bridge.service
sudo systemctl disable ircddbgatewayd.service
sudo systemctl disable p25parrot.service
sudo systemctl disable ysfparrot.service
sudo systemctl disable nxdnparrot.service
sudo systemctl disable monit.service
sudo systemctl disable webproxy.service

rm /var/log/mmdvm/*

sudo cat > /lib/systemd/system/http.server-dvs.service <<- "EOF"
[Unit]
Description=PHP http.server.DVS
After=network.target

[Service]
User=root
#ExecStartPre=/bin/sleep 30
# Modify for different other port
ExecStart=php -S 0.0.0.0:80 -t /var/www/dvs/

[Install]
WantedBy=multi-user.target



EOF
#
systemctl daemon-reload 
