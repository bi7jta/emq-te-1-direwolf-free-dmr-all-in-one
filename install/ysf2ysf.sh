#!/bin/bash

if [ -d "/var/log/ysf_bridge1" ]
then
   rm -r /var/log/ysf_bridge1
fi
   mkdir /var/log/ysf_bridge1

if [ -d "/var/log/ysf_bridge2" ]
then
   rm -r /var/log/ysf_bridge2
fi
   mkdir /var/log/ysf_bridge2

if [ -d "/opt/ysf_bridge1" ]
then
   rm -r /opt/ysf_bridge1
fi
if [ -d "/opt/ysf_bridge2" ]
then
   rm -r /opt/ysf_bridge2
fi

cd /opt
git clone https://github.com/iu5jae/ysf_bridge.git ysf_bridge1
cp ysf_bridge1 ysf_bridge2 -r

sudo chmod +x /opt/ysf_bridge1/*
sudo chmod +x /opt/ysf_bridge2/*
sudo sed -i "s/log_file =.*/log_file = \/var\/log\/ysf_bridge1\/ysf_bridge.log/g" /opt/ysf_bridge1/ysf_bridge.ini
sudo sed -i "s/log_file =.*/log_file = \/var\/log\/ysf_bridge2\/ysf_bridge.log/g" /opt/ysf_bridge2/ysf_bridge.ini
sudo sed -i "s/ycs_ID =.*/ycs_ID = 00/g" /opt/ysf_bridge1/ysf_bridge.ini
sudo sed -i "s/ycs_ID =.*/ycs_ID = 00/g" /opt/ysf_bridge2/ysf_bridge.ini
sudo sed -i "s/port =.*/port = 42000/g" /opt/ysf_bridge1/ysf_bridge.ini
sudo sed -i "s/port =.*/port = 42000/g" /opt/ysf_bridge2/ysf_bridge.ini
sudo sed -i "s/ysfgateway_ID =.*/ysfgateway_ID = 1234567/g" /opt/ysf_bridge1/ysf_bridge.ini
sudo sed -i "s/ysfgateway_ID =.*/ysfgateway_ID = 1234567/g" /opt/ysf_bridge2/ysf_bridge.ini

sudo cat > /lib/systemd/system/ysf2ysf1.service <<- "EOF"
[Unit]
Description=ysf_bridge1
After=network.target

[Service]
ExecStart=/usr/bin/python3 /opt/ysf_bridge1/ysf_bridge.py /opt/ysf_bridge1/ysf_bridge.ini
WorkingDirectory=/opt/ysf_bridge1/
Restart=on-failure

[Install]
WantedBy=multi-user.target



EOF

sudo cat > /lib/systemd/system/ysf2ysf2.service <<- "EOF"
[Unit]
Description=ysf_bridge2
After=network.target

[Service]
ExecStart=/usr/bin/python3 /opt/ysf_bridge2/ysf_bridge.py /opt/ysf_bridge2/ysf_bridge.ini
WorkingDirectory=/opt/ysf_bridge2/
Restart=on-failure

[Install]
WantedBy=multi-user.target



EOF

systemctl daemon-reload



