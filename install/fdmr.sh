#!/bin/bash
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/fdmr/-/raw/main/install.sh)"
######################################################################################

cronedit.sh '*/5 * * * *' 'sh /opt/FDMR-Monitor/sysinfo/graph.sh' remove &&
cronedit.sh '*/2 * * * *' 'sh /opt/FDMR-Monitor/sysinfo/cpu.sh' remove &&
cronedit.sh '*/5 * * * *' 'sh /etc/freedmr/hbmon/sysinfo/graph.sh' remove &&
cronedit.sh '*/2 * * * *' 'sh /etc/freedmr/hbmon/sysinfo/cpu.sh' remove &&
cronedit.sh '* */24 * * *' 'rm /etc/freedmr/hbmon/data/*' remove &&
cronedit.sh '* */24 * * *' 'rm /opt/FDMR-Monitor/data/*' remove &&
cronedit.sh '* */24 * * *' 'rm /opt/FDMR-Monitor2/data/*' remove
 
sudo systemctl stop mariadb.service
sudo systemctl disable mariadb.service
sudo systemctl stop fdmr_mon2.service
sudo systemctl disable fdmr_mon2.service
sudo systemctl stop http.server-fdmr2.service
sudo systemctl disable http.server-fdmr2.service
sudo systemctl stop fdmr_mon.service
sudo systemctl disable fdmr_mon.service
sudo systemctl stop http.server-fdmr.service
sudo systemctl disable http.server-fdmr.service



