#!/bin/bash
if [ -d "/opt/D-APRS" ]
then
   rm -r /opt/D-APRS
 #echo "found file"

fi
if [ -d "/opt/kalibrate-rtl" ]
then
   rm -r /opt/kalibrate-rtl
 #echo "found file"

fi
if [ -d "/opt/YSF2DMRGW" ]
then
   rm -r /opt/YSF2DMRGW
 #echo "found file"

fi
if [ -d "/opt/YSF2DMR" ]
then
   rm -r /opt/YSF2DMR
 #echo "found file"

fi
if [ -d "/var/log/ysf2dmr" ]
then
  rm -r /var/log/ysf2dmr
fi
if [ -d "/opt/direwolf" ]
then
   rm -r /opt/direwolf
 #echo "found file"

fi
if [ -d "/opt/FDMR-Monitor" ]
then
   rm -r /opt/FDMR-Monitor
 #echo "found file"

fi
if [ -d "/opt/MMDVM_CM" ]
then
   rm -r /opt/MMDVM_CM
 #echo "found file"

fi
if [ -d "/opt/YSFGateway2" ]
then
   rm -r /opt/YSFGateway2
 #echo "found file"

fi
if [ -d "/opt/shell-aprs" ]
then
   rm -r /opt/shell-aprs
 #echo "found file"

fi
if [ -d "/opt/rtl-sdr" ]
then
   rm -r /opt/rtl-sdr
 #echo "found file"

fi
if [ -d "/opt/direwolf_webstat" ]
then
   rm -r /opt/direwolf_webstat
 #echo "found file"

fi
if [ -d "/opt/FreeDMR" ]
then
   rm -r /opt/FreeDMR
 #echo "found file"

fi
if [ -d "/opt/MMDVMHost" ]
then
   rm -r /opt/MMDVMHost
 #echo "found file"

fi
if [ -d "/opt/pymultimonaprs" ]
then
   rm -r /opt/pymultimonaprs
 #echo "found file"

fi
if [ -d "/opt/WSYSFDash" ]
then
   rm -r /opt/WSYSFDash
 #echo "found file"

fi
if [ -d "/opt/DMRGateway" ]
then
   rm -r /opt/DMRGateway
 #echo "found file"

fi
if [ -d "/opt/MMDVMHost-Websocketboard" ]
then
   rm -r /opt/MMDVMHost-Websocketboard
 #echo "found file"

fi
if [ -d "/opt/pYSFReflector3" ]
then
   rm -r /opt/pYSFReflector3
 #echo "found file"

fi
if [ -d "/opt/ionsphere" ]
then
   rm -r /opt/ionsphere
 #echo "found file"

fi
if [ -d "/opt/multimon-ng" ]
then
   rm -r /opt/multimon-ng
 #echo "found file"

fi
if [ -d "/opt/noip" ]
then
   rm -r /opt/noip
 #echo "found file"

fi
if [ -d "/usr/local/bin/noip2" ]
then
   rm -r /usr/local/bin/noip2
   #echo "found file"

fi
if [ -d "/opt/data-files" ]
then
   rm -r /opt/data-files
   #echo "found file"

fi
if [ -f "/usr/local/etc/no-ip2.conf" ]
then
  rm /usr/local/etc/no-ip2.conf
  #echo "found file"

fi
if [ -f "/tmp/no-ip2.conf" ]
then
   rm /tmp/no-ip2.conf
 #echo "found file"

fi
if [ -d "/var/log/FreeDMR" ]
then
  sudo rm -r /var/log/FreeDMR
 #echo "found file"

fi
if [ -d "/var/log/mmdvmh" ]
then
  sudo rm -r /var/log/mmdvmh
 #echo "found file"

fi
if [ -f "/var/log/fail2ban.*" ]
then
  sudo rm /var/log/fail2ban*
 #echo "found file"
else
  echo "file not found"
fi
if ls /var/log/*.1 &>/dev/null; then
  sudo rm /var/log/*.1*
  #echo "found file"
fi

##############
