#!/bin/bash
archivo="/etc/sudoers"
palabra="www-data ALL=(ALL) NOPASSWD:ALL"

if grep -Fxq "$palabra" "$archivo"; then
  echo "Line already exists"
else
  echo "$palabra" >> "$archivo"
  echo "Line added"
fi
if [ -d "/var/www/dvs" ]
then
  rm -r /var/www/dvs
fi
mkdir /var/www/dvs
if ! [ -f "/opt/wdp3" ]
then
sudo cat > /opt/wdp3 <<- "EOFX"
##########################################
# Select number port, DVSwitch Dashboard #
##########################################

Web-Dashboar-Port: 80

EOFX
fi
sudo wget -O /var/www/dvs/DVS-Dash.zip usrp.duckdns.org/DVS_Dashboard/DVS.zip &&
cd /var/www/dvs
sudo unzip -o DVS-Dash.zip &&
sed -i "s/'UTC'/'Africa\/Lagos'/"  /var/www/dvs/include/*.php
sed -i "s/www\/html/www\/dvs/g" /var/www/dvs/include/* &&
#sed -i "s/usrp.duckdns.org\/user.csv/datafiles.ddns.net:8888\/user.csv/g" /var/www/dvs/include/* &&
sed -i "s/Dashboard based on Pi-Star Dashboard, © Andy Taylor.*/Dashboard based on Pi-Star Dashboard, © Andy Taylor (MW0MWZ) and adapted to DVSwitch by SP2ONG<br> <a title=\"Raspbian Proyect by HP3ICC © <?php \$cdate=date(\"Y\"); if (\$cdate > \"2018\") {\$cdate=\"2018-\".date(\"Y\");} echo \$cdate; ?>\" target=\"_blank\" href=https:\/\/gitlab.com\/hp3icc\/emq-TE1\/>Proyect: emq-TE1ws+<\/a><\/span><\/center>/" /var/www/dvs/index.php &&
sed -i "s/<meta name=\"Description\".*/<meta name=\"Description\" content=\"Dashboard based on Pi-Star Dashboard, © Andy Taylor (MW0MWZ) and adapted to DVSwitch by SP2ONG\" \/>/" /var/www/dvs/index.php &&
cp /var/www/dvs/index.php /opt/index-dvs.txt
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/menu/menu-dvs)"
sed -i "s/www\/html/www\/dvs/g" /usr/local/sbin/update-config.sh
sed -i "s/www\/html/www\/dvs/g" /var/lib/dpkg/info/dvswitch*
#
sed -i "s/8080/9009/g" /var/www/dvs/include/*.*
if [ ! -d "/var/www/dvs/qrz_photo" ]
then
  sudo mkdir /var/www/dvs/qrz_photo
fi
sudo chmod o+w /var/www/dvs/qrz_photo
if grep -q "8080" /lib/systemd/system/webproxy.service; then
    systemctl stop webproxy.service
    sed -i "s/8080/9009/g" /opt/Web_Proxy/proxy.js
    sed -i "s/8080/9009/g" /lib/systemd/system/webproxy.service
    systemctl daemon-reload
fi
if ! sudo systemctl status webproxy.service |grep "service; enabled;" >/dev/null 2>&1
   then 
   sudo systemctl enable webproxy.service
fi
if ! systemctl status webproxy.service |grep "Active: active" >/dev/null 2>&1
  then 
   sudo systemctl stop webproxy.service
   sudo systemctl start webproxy.service
fi
if ! sudo systemctl status monit.service |grep "service; enabled;" >/dev/null 2>&1
   then 
   sudo systemctl enable monit.service
fi
if ! systemctl status monit.service |grep "Active: active" >/dev/null 2>&1
  then 
     sudo systemctl stop monit.service
     sudo systemctl start monit.service
fi
#
if [ ! -f "/lib/systemd/system/http.server-dvs.service" ]
then
  sudo cat > /lib/systemd/system/http.server-dvs.service <<- "EOF"
[Unit]
Description=PHP http.server.DVS
After=network.target

[Service]
User=root
#ExecStartPre=/bin/sleep 30
# Modify for different other port
ExecStart=php -S 0.0.0.0:80 -t /var/www/dvs/

[Install]
WantedBy=multi-user.target



EOF
#
fi
if sudo systemctl status lighttpd.service |grep "service; enabled;" >/dev/null 2>&1
   then 
   sudo systemctl disable lighttpd.service
   sudo systemctl stop lighttpd.service

fi 
variable80=$(grep "Web-Dashboar-Port:" /opt/wdp3 | grep -Eo '[A.0-9]{1,9}')
if [ -z "$variable80" ]
then variable80=80

fi
sudo sed -i "s/ExecStart=.*/ExecStart=php -S 0.0.0.0:$variable80 -t \/var\/www\/dvs\//g"  /lib/systemd/system/http.server-dvs.service &&
systemctl daemon-reload &&
if sudo systemctl status http.server-dvs.service |grep "service; enabled;" >/dev/null 2>&1
   then 
   sudo systemctl stop http.server-dvs.service
   sudo systemctl start http.server-dvs.service

fi