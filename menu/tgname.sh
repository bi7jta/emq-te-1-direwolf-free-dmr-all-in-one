#!/bin/bash
if [[ $EUID -ne 0 ]]; then
        whiptail --title "sudo su" --msgbox "requiere ser usuario root , escriba (sudo su) antes de entrar a menu / requires root user, type (sudo su) before entering menu" 0 50
        exit 0
fi

###########
cat > /opt/tg-name-fdmr.py <<- "EOF"
import csv
import requests

# URL del archivo CSV
url = "https://freedmr.cymru/talkgroups/talkgroup_ids.csv"

# Descargar el archivo CSV
response = requests.get(url)
content = response.content.decode("utf-8")

# Leer el archivo CSV original
csv_reader = csv.reader(content.splitlines())
header = next(csv_reader)  # Leer la primera fila como encabezado

# Ruta de archivo de salida
output_file_path = "/opt/MMDVMHost-Websocketboard/html/data/TG_List.csv"

# Almacenar los datos en una lista
data = []

# Procesar los datos del archivo original y agregarlos a la lista
for row in csv_reader:
    data.append([int(row[0]), row[1], int(row[2])])

# Agregar la línea adicional
data.append([9, "Dial-TG TG9"])
data.append([6, "XLX TG6"])
data.append([2, "Local TG2"])
data.append([9990, "Parrot TG9990"])

# Ordenar la lista por el valor de la columna "id"
data.sort(key=lambda x: x[0])

# Abrir el archivo de salida en modo escritura
with open(output_file_path, "w", newline="") as output_file:
    csv_writer = csv.writer(output_file)
    
    # Escribir la primera fila de encabezado
    csv_writer.writerow(["DMR", "2", "id", "callsign tgid"])
    
    # Escribir los datos en el nuevo formato
    for row in data:
        if len(row) == 3:
            csv_writer.writerow(["DMR", "2", row[0], f"{row[1]} {row[2]}"])
        else:
            csv_writer.writerow(["DMR", "2", row[0], row[1]])

print("Proceso completado. Archivo de salida generado en:", output_file_path)

EOF
cat > /opt/tg-name-bm.py <<- "EOF"
import csv
import requests

# URL del archivo de texto
url = "http://www.pistar.uk/downloads/TGList_BM.txt"

# Descargar el archivo de texto
response = requests.get(url)
content = response.content.decode("utf-8")

# Dividir el contenido del archivo en líneas
lines = content.splitlines()

# Ruta del archivo de salida
output_file_path = "/opt/MMDVMHost-Websocketboard/html/data/TG_List.csv"

# Almacenar los datos en una lista
data = []

# Procesar los datos del archivo original y agregarlos a la lista
for line in lines:
    if not line.startswith('#'):
        parts = line.split(';')
        if len(parts) >= 4:
            data.append([int(parts[0]), parts[2], parts[3]])

# Agregar líneas adicionales
data.append([9990, "Parrot", "TG9990"])
data.append([6, "XLX", "TG6"])

# Ordenar la lista por el valor de la columna "id"
data.sort(key=lambda x: x[0])

# Abrir el archivo de salida en modo escritura
with open(output_file_path, "w", newline="") as output_file:
    csv_writer = csv.writer(output_file)
    
    # Escribir la primera fila de encabezado
    csv_writer.writerow(["DMR", "2", "id", "callsign tgid"])
    
    # Escribir los datos en el nuevo formato
    for row in data:
        csv_writer.writerow(["DMR", "2", row[0], row[1] + " " + row[2]])

print("Proceso completado. Archivo de salida generado en:", output_file_path)

EOF
cat > /opt/tg-name-ds.py <<- "EOF"
import csv
import requests

# URL del primer archivo de texto
url1 = "http://www.pistar.uk/downloads/TGList_BM.txt"
# URL del segundo archivo CSV
url2 = "https://freedmr.cymru/talkgroups/talkgroup_ids.csv"

# Descargar el contenido del primer archivo de texto
response1 = requests.get(url1)
content1 = response1.content.decode("utf-8")

# Descargar el contenido del segundo archivo CSV
response2 = requests.get(url2)
content2 = response2.content.decode("utf-8")

# Dividir las líneas de ambos contenidos
lines1 = content1.splitlines()
lines2 = content2.splitlines()

# Almacenar los datos de url1 en una lista
data_url1 = []

# Procesar los datos del primer archivo y agregarlos a la lista
for line in lines1:
    if not line.startswith('#'):
        parts = line.split(';')
        if len(parts) >= 4:
            data_url1.append([int(parts[0]), parts[2], parts[3]])

# Ordenar la lista de url1 por el valor de la tercera columna (alfanumérico)
data_url1.sort(key=lambda x: x[2])

# Almacenar los datos de url2 en una lista
data_url2 = []

# Procesar los datos del segundo archivo y agregarlos a la lista
csv_reader = csv.reader(lines2)
next(csv_reader)  # Ignorar la primera fila de encabezado

for row in csv_reader:
    if len(row) == 3:
        data_url2.append([int(row[0]), row[1], int(row[2])])
    else:
        data_url2.append([int(row[0]), row[1]])

# Ordenar la lista de url2 por el valor de la tercera columna (numérico)
data_url2.sort(key=lambda x: x[2])

# Ruta del archivo de salida
output_file_path = "/opt/MMDVMHost-Websocketboard/html/data/TG_List.csv"

# Abrir el archivo de salida en modo escritura
with open(output_file_path, "w", newline="") as output_file:
    csv_writer = csv.writer(output_file)

    # Escribir la primera fila de encabezado
    csv_writer.writerow(["DMR", "1", "id", "callsign tgid"])

    # Escribir los datos de url1 en el formato deseado
    for row in data_url1:
        if row[1]:
            formatted_callsign = f"BM / {row[1]} {row[2]}"
        else:
            formatted_callsign = f"BM / {row[2]}"
        csv_writer.writerow(["DMR", "1", row[0], formatted_callsign])

    # Escribir las líneas adicionales de url1
    csv_writer.writerow(["DMR", "1", 9990, "BM / Parrot TG9990"])
    csv_writer.writerow(["DMR", "1", 6, "slot1 / XLX TG6"])

    # Escribir las líneas adicionales de url2
    csv_writer.writerow(["DMR", "2", 9990, "FDMR / Parrot TG9990"])
    csv_writer.writerow(["DMR", "2", 6, "slot2 / XLX TG6"])

    # Escribir los datos de url2 en el formato deseado
    for row in data_url2:
        if len(row) == 3:
            formatted_callsign = f"FDMR / {row[1]} {row[2]}"
        else:
            formatted_callsign = f"FDMR / {row[1]}"
        csv_writer.writerow(["DMR", "2", row[0], formatted_callsign])

print("Proceso completado. Archivo de salida generado en:", output_file_path)


EOF
sudo cat > /opt/ysf-europelink.txt <<- "EOF"
YSF,0,1,PARROT
YSF,0,2,WELCOME
YSF,0,3,WORLD
YSF,0,10,ENGLISH
YSF,0,11,WALLONIE
YSF,0,12,FRANCE
YSF,0,13,OWL-NET
YSF,0,14,WW-SPANISH
YSF,0,15,PANAMA-HUB
YSF,0,16,ALPE-ADRIA
YSF,0,17,DUTCH
YSF,0,18,VLAAMS-BE
YSF,0,19,Almeria
YSF,0,20,FRENCH-BE
YSF,0,21,HISPANO
YSF,0,22,NAVARLIK
YSF,0,23,CANARIAS
YSF,0,24,C4FM-LINK
YSF,0,25,CQ-UK-2
YSF,0,26,CALVIA
YSF,0,27,FUSION-MX
YSF,0,28,RC-FENE
YSF,0,29,VALENCIA
YSF,0,30,GERMAN
YSF,0,31,ZELLO-BM
YSF,0,32,ARG-FREE
YSF,0,33,ARG-ROOM
YSF,0,34,MEXICO
YSF,0,35,CQ-UK
YSF,0,36,DVSCOTLAND
YSF,0,37,GB-NORTH
YSF,0,38,HUBNET
YSF,0,39,DURANGO
YSF,0,40,ITALIAN
YSF,0,41,VENEZUELA
YSF,0,42,IT-MULTIP
YSF,0,43,ZPRV-BE
YSF,0,44,CHIRIQUI
YSF,0,45,RADE-HC
YSF,0,46,REUNION
YSF,0,47,ECUADOR
YSF,0,48,YSF-FR-MLT
YSF,0,49,MIAMI
YSF,0,50,ANDALUCIA
YSF,0,51,ASRABI
YSF,0,52,DE-RAMSES
YSF,0,53,RAMSES-VW
YSF,0,54,VARG
YSF,0,55,US-MIAMI
YSF,0,56,COQUINAS
YSF,0,57,NOROESTE
YSF,0,58,SYSTEMX
YSF,0,59,SALENTONET
YSF,0,60,DE-KUSEL
YSF,0,61,BM-CAT
YSF,0,62,DL-HAMBURG
YSF,0,63,REM
YSF,0,64,LEON-C4FM
YSF,0,65,ADER
YSF,0,70,SKYNET
YSF,0,71,PORTUGAL
YSF,0,75,ALBACETE
YSF,0,77,Granada
YSF,0,78,ELITE
YSF,0,79,ZULURADIO
YSF,0,80,CASTILLA-M
YSF,0,81,FR-MULTI
YSF,0,82,RADHISTEC
YSF,0,83,PT-RICO
YSF,0,84,MALLORCA
YSF,0,86,ARGENTINA
YSF,0,87,Honduras
YSF,0,88,ALCOYANOS
YSF,0,89,PERU
YSF,0,90,Radio-Net
YSF,0,91,DMR-LNK
YSF,0,92,ARCANGEL
YSF,0,93,DE-RUNDE
YSF,0,94,MESSAGES
YSF,0,95,LATINOS
YSF,0,96,EA7URF
YSF,0,97,DATA-ROOM
YSF,0,98,URUGUAY
YSF,0,99,TEST

EOF
sudo cat > /opt/ysf-worldlink.txt <<- "EOF"
YSF,0,1,PARROT
YSF,0,2,WELCOME
YSF,0,10,ENGLISH,English talking room
YSF,0,11,SUPERLINK,Superlink Multilink Brazil
YSF,0,12,Hamr-ARG,Hamradio-ARG
YSF,0,13,CDARIN,ST-VINCENT-CDARIN
YSF,0,14,WW-SPANISH,Spanish-Chat
YSF,0,15,COSTA-RICA,Ticos Fusion Club
YSF,0,16,TIHAS-ROOM,Room Oficial de Radio Club de Costa Rica
YSF,0,17,BELGIUM-FR,The french-speaking Belgians room
YSF,0,19,CQ-CANADA,YSF-Canada-Link
YSF,0,20,AMERICA,America room
YSF,0,21,HAITI,Haiti YSF
YSF,0,22,TEXASNEXUS,C4FM group called Texas Nexus
YSF,0,23,QUAHOGRPTR,Quahog Repeater Network
YSF,0,24,NEWENGLAND,New England YSF
YSF,0,25,CANADA,Canada room
YSF,0,26,QUEBEC,QuÃ©bec FranÃ§ais
YSF,0,27,DX-LINK2,Bringing Amateurs Together
YSF,0,28,USA-DX,USA DX All World
YSF,0,29,JEEP-TALK,Talk About Jeeps And Wheeling
YSF,0,30,PMSA,Pmsa-Fabien
YSF,0,31,LKDVM,LKDVM Lexington Kentucky
YSF,0,32,GERMAN,German speaking
YSF,0,33,IRN-KC1MUV,wellcome www.irn.radio
YSF,0,34,MEXICO,Mexico room
YSF,0,35,CQ-UK-VW,Yaesu Radio VW
YSF,0,36,GEOTXHAMS,Geo TX Hams
YSF,0,37,UFB-NEW-EN,New-England
YSF,0,38,YSF-MAYENN,Fusion-Mayenne
YSF,0,39,COLOMBIA,Colombia room
YSF,0,40,CUBA-DV,Cuban Digital Voice Club
YSF,0,41,TRUJILLO,TRUJILLO OA2
YSF,0,42,MARDEL,MAR DEL PLATA
YSF,0,43,BULGARIA,Bulgaria Room
YSF,0,44,CHIRIQUI,Chiriqui Link Panama
YSF,0,60,AUSTRALIA,Australian room
YSF,0,61,NEWZEALAND,New Zealand room
YSF,0,70,WW-DUTCH,Dutch chat
YSF,0,71,VLAAMS-BE,Vlaams Belgie
YSF,0,72,BRASIL,BRASIL room
YSF,0,73,AUTXUSA,Austin Texas Hispanic Amateur Radio Club
YSF,0,74,CQ-OLEBOYZ,All Hams Welcome To Our Room
YSF,0,75,DX-LINK-1,DX-LINK-1
YSF,0,80,INDIA,India chat room
YSF,0,81,CRAS,RC-CRAS
YSF,0,86,ARGENTINA,ARGENTINA
YSF,0,87,ARG-DIG,Argentina Digital
YSF,0,88,PHILIPPINE,Philippines chat room
YSF,0,89,CRETE-YSF,GR CRETE YSF CLUB
YSF,0,98,URUGUAY,Uruguay chat room
YSF,0,99,TEST,Testing Room

EOF
sudo cat > /opt/dmr5name.py <<- "EOF"
import csv
import requests

# Archivo de origen dmrc
archivo_entrada_dmr = '/opt/tg-dmrc'

# Archivo de origen tg-tgif
archivo_entrada_tg_tgif = '/opt/tg-tgif'

# Archivo de origen tg-systemx
archivo_entrada_tg_systemx = '/opt/tg-systemx'

# Archivo de origen tg-bm
archivo_tg_bm = '/opt/tg-bm'

# Archivo de origen tg-fdmr
archivo_tg_fdmr = '/opt/tg-fdmr'

# URL del primer archivo de texto
url1 = "http://www.pistar.uk/downloads/TGList_BM.txt"
# URL del segundo archivo CSV
url2 = "https://freedmr.cymru/talkgroups/talkgroup_ids.csv"

# Leer el archivo de dmrc
with open(archivo_entrada_dmr, 'r') as archivo_dmr:
    lineas_dmr = archivo_dmr.readlines()

# Leer el archivo de tg-tgif
with open(archivo_entrada_tg_tgif, 'r') as archivo_tg_tgif:
    lineas_tg_tgif = archivo_tg_tgif.readlines()

# Leer el archivo de tg-systemx
with open(archivo_entrada_tg_systemx, 'r') as archivo_tg_systemx:
    lineas_tg_systemx = archivo_tg_systemx.readlines()

# Leer el archivo tg-bm
with open(archivo_tg_bm, 'r') as archivo_tg_bm:
    lineas_tg_bm = archivo_tg_bm.readlines()

# Leer el archivo tg-fdmr
with open(archivo_tg_fdmr, 'r') as archivo_tg_fdmr:
    lineas_tg_fdmr = archivo_tg_fdmr.readlines()

# Descargar el contenido del primer archivo de texto
response1 = requests.get(url1)
content1 = response1.content.decode("utf-8")

# Descargar el contenido del segundo archivo CSV
response2 = requests.get(url2)
content2 = response2.content.decode("utf-8")

# Dividir las líneas de ambos contenidos
lines1 = content1.splitlines()
lines2 = content2.splitlines()

# Almacenar los datos de url1 en una lista
data_url1 = []

# Procesar los datos del primer archivo y agregarlos a la lista
for line in lines1:
    if not line.startswith('#'):
        parts = line.split(';')
        if len(parts) >= 4:
            data_url1.append([int(parts[0]), parts[2], parts[3]])

# Ordenar la lista de url1 por el valor de la tercera columna (alfanumérico)
data_url1.sort(key=lambda x: x[2])

# Almacenar los datos de url2 en una lista
data_url2 = []

# Procesar los datos del segundo archivo y agregarlos a la lista
csv_reader = csv.reader(lines2)
next(csv_reader)  # Ignorar la primera fila de encabezado

for row in csv_reader:
    if len(row) == 3:
        data_url2.append([int(row[0]), row[1], int(row[2])])
    else:
        data_url2.append([int(row[0]), row[1]])

# Ordenar la lista de url2 por el valor de la tercera columna (numérico)
data_url2.sort(key=lambda x: x[2])

# Combinar las líneas de todos los archivos
lineas_filtradas = []

# Procesar las líneas de dmrc
for linea in lineas_dmr:
    partes = linea.strip().split('\t')
    if len(partes) > 0 and len(partes[0]) <= 6 and partes[0].isdigit():
        # Agregar el número 4 al inicio y rellenar con ceros
        primera_columna = '4{:06d}'.format(int(partes[0]))
        plataforma = 'DMRC /'
        nueva_linea = 'DMR,2,{},{} {}'.format(primera_columna, plataforma, partes[1])
        lineas_filtradas.append(nueva_linea)

# Procesar las líneas de tg-tgif
for linea in lineas_tg_tgif:
    partes = linea.strip().split('\t')
    if len(partes) > 0 and len(partes[0]) <= 6 and partes[0].isdigit():
        # Agregar el número 3 al inicio y rellenar con ceros
        primera_columna = '3{:06d}'.format(int(partes[0]))
        plataforma = 'TGIF /'
        nueva_linea = 'DMR,2,{},{} {}'.format(primera_columna, plataforma, partes[1])
        lineas_filtradas.append(nueva_linea)

# Procesar las líneas de tg-systemx
for linea in lineas_tg_systemx:
    partes = linea.strip().split('\t')
    if len(partes) > 0 and len(partes[0]) <= 6 and partes[0].isdigit():
        # Agregar el número 5 al inicio y rellenar con ceros
        primera_columna = '5{:06d}'.format(int(partes[0]))
        plataforma = 'SX /'
        nueva_linea = 'DMR,2,{},{} {}'.format(primera_columna, plataforma, partes[1])
        lineas_filtradas.append(nueva_linea)

# Procesar las líneas de tg-bm y aplicar el formato de url1
for i, row in enumerate(lineas_tg_bm):
    partes = row.strip().split('\t')
    if len(partes) > 0 and len(partes[0]) <= 6 and partes[0].isdigit():
        primera_columna = '1{:06d}'.format(int(partes[0]))
        plataforma = 'BM /'
        nueva_linea = 'DMR,2,{},{} {}'.format(primera_columna, plataforma, partes[1])
        lineas_filtradas.append(nueva_linea)

# Procesar las líneas de tg-fdmr y aplicar el formato de url2
for i, row in enumerate(lineas_tg_fdmr):
    partes = row.strip().split('\t')
    if len(partes) > 0 and len(partes[0]) <= 6 and partes[0].isdigit():
        primera_columna = '2{:06d}'.format(int(partes[0]))
        plataforma = 'FDMR /'
        nueva_linea = 'DMR,2,{},{} {}'.format(primera_columna, plataforma, partes[1])
        lineas_filtradas.append(nueva_linea)

# Agregar las líneas de url1
for row in data_url1:
    primera_columna = '1{:06d}'.format(row[0])
    plataforma = 'BM /'
    nueva_linea = 'DMR,2,{},{} {}'.format(primera_columna, plataforma, row[1])
    lineas_filtradas.append(nueva_linea)

# Agregar las líneas de url2
for row in data_url2:
    primera_columna = '2{:06d}'.format(row[0])
    plataforma = 'FDMR /'
    nueva_linea = 'DMR,2,{},{} {}'.format(primera_columna, plataforma, row[1])
    lineas_filtradas.append(nueva_linea)

# Generar el archivo de salida
with open('/opt/MMDVMHost-Websocketboard/html/data/TG_List.csv', 'w') as archivo_salida:
    for linea in lineas_filtradas:
        archivo_salida.write(linea + '\n')

print("Proceso completado. Archivo de salida '/opt/MMDVMHost-Websocketboard/html/data/TG_List.csv' generado con líneas de todos los archivos.")

EOF

############
sudo cat > /bin/menu-uptg <<- "EOF"
#!/bin/bash
if [[ $EUID -ne 0 ]]; then
        whiptail --title "sudo su" --msgbox "requiere ser usuario root , escriba (sudo su) antes de entrar a menu / requires root user, type (sudo su) before entering menu" 0 50
        exit 0
fi

while : ; do
choix=$(whiptail --title "Raspbian Proyect HP3ICC / Dashboar TG Name" --menu "Select list TG name for dashboard mmdvmhost.
" 17 59 7 \
1 " TG Name Branmeister " \
2 " TG Name FreeDMR " \
3 " Duplex mode, S1=BM / S2=FDMR " \
4 " YSF DGiD Name WorldLink " \
5 " YSF DGiD Name EuropeLink " \
6 " DMRGateway 5 Network (BM,FDMR,TGIF,DMRC,SYSTEMX) " \
7 " Menu Principal " 3>&1 1>&2 2>&3)
exitstatus=$?
#on recupere ce choix
#exitstatus=$?
if [ $exitstatus = 0 ]; then
    echo "Your chosen option:" $choix
else
    echo "You chose cancel."; break;
fi
# case : action en fonction du choix
case $choix in
1)
python3 /opt/tg-name-bm.py ;;
2)
python3 /opt/tg-name-fdmr.py ;;
3)
python3 /opt/tg-name-ds.py ;;
4)
sudo cat /opt/ysf-worldlink.txt >> /opt/MMDVMHost-Websocketboard/html/data/TG_List.csv  ;;
5)
sudo cat /opt/ysf-europelink.txt >> /opt/MMDVMHost-Websocketboard/html/data/TG_List.csv ;;
6)
wget https://gitlab.com/hp3icc/emq-TE1/-/raw/main/menu/tg-dmrc -O /opt/tg-dmrc
wget https://gitlab.com/hp3icc/emq-TE1/-/raw/main/menu/tg-tgif -O /opt/tg-tgif
wget https://gitlab.com/hp3icc/emq-TE1/-/raw/main/menu/tg-systemx -O /opt/tg-systemx
wget https://gitlab.com/hp3icc/emq-TE1/-/raw/main/menu/tg-fdmr -O /opt/tg-fdmr
wget https://gitlab.com/hp3icc/emq-TE1/-/raw/main/menu/tg-bm -O /opt/tg-bm
python3 /opt/dmr5name.py ;;
7)
break;
esac
done
exit 0


EOF
#
chmod +x /bin/menu-uptg 
menu-uptg

