#!/bin/sh
 
sudo systemctl stop analog_bridge.service
sudo systemctl stop mmdvm_bridge.service
sudo systemctl stop ysfgateway.service
sudo systemctl stop ysfparrot.service
sudo systemctl stop nxdngateway.service
sudo systemctl stop nxdnparrot.service
sudo systemctl stop p25gateway.service
sudo systemctl stop p25parrot.service
sudo systemctl stop quantar_bridge.service
sudo systemctl stop ircddbgatewayd.service
sudo systemctl stop md380-emu.service
#
rm /lib/systemd/system/analog_bridge.service
rm /lib/systemd/system/mmdvm_bridge.service
rm /lib/systemd/system/ysfgateway.service
rm /lib/systemd/system/ysfparrot.service
rm /lib/systemd/system/nxdngateway.service
rm /lib/systemd/system/nxdnparrot.service
rm /lib/systemd/system/p25gateway.service
rm /lib/systemd/system/p25parrot.service
rm /lib/systemd/system/quantar_bridge.service
rm /lib/systemd/system/ircddbgatewayd.service
rm /lib/systemd/system/md380-emu.service
cd /lib/systemd/system/
wget https://gitlab.com/hp3icc/emq-TE1/-/raw/main/service/analog_bridge.service
wget https://gitlab.com/hp3icc/emq-TE1/-/raw/main/service/mmdvm_bridge.service
wget https://gitlab.com/hp3icc/emq-TE1/-/raw/main/service/ysfgateway.service
wget https://gitlab.com/hp3icc/emq-TE1/-/raw/main/service/ysfparrot.service
wget https://gitlab.com/hp3icc/emq-TE1/-/raw/main/service/nxdngateway.service
wget https://gitlab.com/hp3icc/emq-TE1/-/raw/main/service/nxdnparrot.service
wget https://gitlab.com/hp3icc/emq-TE1/-/raw/main/service/p25gateway.service
wget https://gitlab.com/hp3icc/emq-TE1/-/raw/main/service/p25parrot.service
wget https://gitlab.com/hp3icc/emq-TE1/-/raw/main/service/quantar_bridge.service
wget https://gitlab.com/hp3icc/emq-TE1/-/raw/main/service/ircddbgatewayd.service
wget https://gitlab.com/hp3icc/emq-TE1/-/raw/main/service/md380-emu.service
#
sudo find /lib/systemd/system/ -type f -name 'analog_bridge*' -exec sed -i 's/ExecStartPre=\/bin\/sh -c '\''until ping/#ExecStartPre=\/bin\/sh -c '\''until ping/' {} +
sudo find /lib/systemd/system/ -type f -name 'md380-emu*' -exec sed -i 's/ExecStartPre=\/bin\/sh -c '\''until ping/#ExecStartPre=\/bin\/sh -c '\''until ping/' {} +
sudo find /lib/systemd/system/ -type f -name 'mmdvm_bridge*' -exec sed -i 's/ExecStartPre=\/bin\/sh -c '\''until ping/#ExecStartPre=\/bin\/sh -c '\''until ping/' {} +

sudo systemctl daemon-reload

echo "Reiniciando  / Restarting "
sudo reboot