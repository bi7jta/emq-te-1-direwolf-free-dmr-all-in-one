# emq-TE1+

<img src="https://gitlab.com/hp3icc/emq-TE1/-/raw/main/img/IMG_3210.JPG" width="500" height="500">

hp3icc Proyecto Todo en uno , MMDVMHost ,Direwolf con Dashboard , Dvswitch, YSFReflector , YSF2DMR , HBLink3 mod FDMR-Monitor Self-Service , FreeDMR , NoIP , Dashboard Websock

Dashboard html sobre websock , soporte GPSD , CM108 .

Continuamente todo el contenido publicado aqui, es actualizado , si esta observando atravez de un link compartido , dirijase al link principal :

https://gitlab.com/hp3icc/emq-TE1

#

El proyecto Todo en uno (emq-TE1ws), es una compilación que reúne aplicaciones de diferentes desarrolladores, enfocadas para uso de radioaficionados. Constante mente se trabaja en mejoras y actualizaciones, a medida que los desarrolladores de las aplicaciones incluidas lanzan nuevas versiones.
Todas las aplicaciones compiladas en esta imagen son 100% operativas, solo debe configurar sus parámetros e iniciar las aplicaciones que desee utilizar , según la capacidad y disponibilidad de su hardware .

Cualquier información sobre como configurar sus parámetros en las diferentes aplicaciones compiladas en esta imagen, debe dirigirse a los diferentes sitios de soporte de cada aplicación o desarrollador.

Especial agradecimiento al colega y amigo TI4OP Oscar , por sus aportes y revisiones de los scripts, para la creación del Bash de instalación de esta imagen.

#

Listado de aplicaciones de radioaficionados, que incluye la imagen y bash de instalación :

* Python APRS Beacon

* Python APRS WX

* Direwolf (Dashboard, RTL, GPSD & CM108)

* MMDVMHost

* MMDVMCal

* DMRGateway

* pYSFReflector (With APRS Gateway) 

* NXDN2DMR 

* YSF2DMR 

* YSF2YSF (YSFBridge)

* P25Reflector

* P25Reflector-Dashboard

* P25 < Bridge > DMR/XLX

* Dvswitch (Dashboard EasyUserInterface by DS5QDR)

* HBLink3 (Private-Call)

* HBmonitor for HBlink

* HBmonitor2 For HBlink

* HBJson for hblink

* FreeDMR (No Docker)

* FDMR-Monitor by OA4DOA (version Self-service for FreeDMR)

* FDMR-Monitor by CS8ABG (version Self-service for Freedmr)

* NoIP

* GoTTY

* Balena WiFi Connect (Only on Raspberry device)

* Linux Network Manager (Only on Raspberry device)


# Notas 

* Todos los Dashboard, estan preconfigurados a puerto http 80 , pero desde el menú puede cambiar al puerto 8000, 8080 , o cualquier otro de su preferencia .

* se agrega Librerias y aplicacion de gps y GPSD

* Se agrega a Dashboard de MMMDMVHost, lista mundial de nombres de TG de las redes DMR: brandmeister, FreeDMR, TGiF, DMR-Central y System-X (FreeStar) .

* Se agrega a Dashboard de MMMDMVHost, listado de nombres de salas del Proyecto Treehouse: WorldLink y EUROPELINK según el DG-ID correspondiente .


* Compatible con PC, VPS, maquinas virtuales o Raspberry pi (zero , P2 , P3 y P4), algunas aplicaciones requieren mayor performance de Hardware que otras según su desarrollador.  

#

# Descargar imagen de micro sd para Raspberry:

si posee equipo raspbery , puede descargar y utilizar la imagen preconfigurada  lista para cargar en su memoria micro sd , para esto solo deberá descargar el archivo de la imagen preconfigurada para raspberry, descomprimir el archivo .zip y cargar en su memoria micro sd utilizando herramientas como BalenaEtcher , Rufus o cualquier otra herramienta para cargar el archivo .img a la memoria micro sd.

 Puede descargar la imagen para raspberry desde cualquiera de los siguientes links:
 
Imagen para raspberry, proyecto emq-TE1 - recomendado para cualquier Raspberry : Zero, Zero-W, Zero2, Zero2 W  B2, B3, B3+, PI4

* <p><a href="https://drive.google.com/u/0/uc?id=17DzFy8i-S1ISvr08QnI8rO7uEUov6K-4&export=download&confirm=t&uuid=4329f6b0-353d-44d1-b08b-2715faa32981" target="_blank">Descargar</a> imagen Raspberry&nbsp;</p>

 Ultima revision de imagen Raspberry: emq-TE1+ Rev:    28/10/2023 

 Raspberry OS Lite Basado en la versión 11 de Debian (bullseye) 03/05/2023
  
 Linux kernel 6.1.21  

#

# Instalación desde terminal:

Puede instalar en su sistema operativo (Ubuntu , raspberry , Debian ) utilizando el Bash de auto instalación desde su consola terminal con permisos de super usuario, importante su sistema operativo debe tener instalado sudo y curl  antes de utilizar el Bash de auto instalación .

* Ultima revision de scrip 28/10/2023 

* Bash de auto instalación :

       apt update && apt upgrade -y

       apt-get install sudo curl -y

       sudo su
       
       bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/install.sh)"


#

# Configuración

Puede configurar desde consola terminal, aplicación cliente ssh o utilizar su navegador Web puerto 8022, ingresando al ip local de su raspberry o hostname.

Ejemplo: 

* 192.168.42.1:8022/  

* emq-te1:8022/ 

Usuario:    pi

Contraseña:  Panama507

si esta configurando desde terminal o clliente ssh, una vez allá iniciado sesión , escriba la palabra:   menu 

De esta forma accederá al listado de aplicaciones incluidas en la compilación y sus configuraciones, recuerde guardar los cambios con la combinación de teclas:     Ctrl + X , posteriormente iniciar o detener la aplicación ya configurada .

A partir de version 12d , se agrega funcion de reinicio automatico del equipo , esto es posible mendiamte prueba de ping al internet cada 1 minuto, esta fumcion esta apagada , si desea utilizar , entre al menu de reinicio de equipo y habilite  

Si desea habilitar más de un Dashboard a la vez, recuerde cambiar los puertos http para evitar conflictos, para esto se incluye dicha opción en el menú de :

* pYSFReflector  
* MMDVMHost
* Dvswitch
* HBLink
* FreeDMR

# Nota importante 

* Si utiliza la imagen pre-compilada para Raspberry , recuerde cambiar la contraseña por una de su preferencia . 

#

# WIFI 

<img src="https://avatars.githubusercontent.com/u/34882892?s=48&v=4" width="75" height="75">
<img src="https://raw.githubusercontent.com/balena-os/wifi-connect/master/docs/images/wifi-connect.png" width="300" height="100">

 A partir de la version emq-TE1-Rev-1.8 o superior, conectar su hotspot a redes wifi sera mucho mas facil, Balena WiFi Connect es una utilidad para establecer dinámicamente la configuración WiFi en un dispositivo Linux a través de un portal cautivo. Las credenciales de WiFi se especifican conectándose con un teléfono móvil o computadora portátil al punto de acceso que crea WiFi Connect

<img src="https://raw.githubusercontent.com/balena-os/wifi-connect/master/docs/images/how-it-works.png" width="375" height="150">

 ya no es necesario descargar archivo adicionales para conectar el hostpot a nuevas redes wifi , solo encienda su hotspot , desde su computadora o telefono conecte a la red wifi con ssid: WiFi Connect

<img src="https://gitlab.com/hp3icc/emq-TE1/-/raw/main/img/IMG_3206.JPG" width="150" height="200"><img src="https://gitlab.com/hp3icc/emq-TE1/-/raw/main/img/IMG_3207.JPG" width="150" height="200"><img src="https://gitlab.com/hp3icc/emq-TE1/-/raw/main/img/IMG_3208.JPG" width="150" height="200">

luego desde el portal cautivo de WiFi Connect , seleccione la red wifi que decea su raspberry se conecte , ingrese la contraseña y click en connect .


# DMRGateway

DMRGateway permite tener nuestro hotspot , dmo , o repetidor dmr; conectado a mas de una red de la modalidad dmr, para esto es necesario seleccionar conexion Gateway en la configuracion del mmdvmhost , y en los radios se debe reconfigurar los numeros de tg segun la red a utilizar .

DMRGateway esta configurado para soportar numeros de tg hasta 6 digitos , si el numero de tg a utilizar tiene menos de 6 digitos , debe completar con ceros hasta llegar a 6 digitos .

Cada servidor DMR conectado a DMRGateway tiene un numero de 1 a 5, que lo identifica y diferencia segun la red a utilizar , y estan distribuidos de la siguiente forma :

  1 Brandmeister
  
  2 FreeDMR
  
  3 TGif Network
  
  4 DMR Central
  
  5 Freestar
  
Cada tg que utilicemos en nuestros radios deve estar cumplir con 6 digitos y estar acompañado del numero de servidor por el cual debe enviarce nuestra transmision.

Ejemplo de configuracion tg en sus radios para diferentes redes:

TG        Red             Nombre TG         DMRGateway

714       FreeDMR         Panama            2000714

7144      FreeDMR         CHIRIQUI LINK     2007144

2147      DMR Central     Regional-EA7      4002147

Cada tg almacenado en nuestros radios debe completar 6 digitos y estar precesido por el numero de servidor dmr a utilizar , cuando se reciban transmiciones de internet a nuestro hotspot , se recibiran de la misma forma, 6 digitos mas el numero de red que estamos recibiendo .

#

# Nota importante, scrip incluye aplicaciones de diferentes desarrolladores , consulte abajo los link de fuentes para para mayor informacion, o soporte de las diferentes aplicaciones.

# Fuentes :

* Python APRS Beacon : https://gitlab.com/hp3icc/python-aprs-beacon

* Python APRS WX : https://gitlab.com/hp3icc/python-aprs-wx

* Direwolf : https://github.com/wb2osz/direwolf

* MMDVMHost : https://github.com/g4klx/MMDVMHost

* MMDVMCal : https://github.com/g4klx/MMDVMCal

* DMRGateway : https://github.com/g4klx/DMRGateway

* P25Reflector : https://github.com/nostar/DVReflectors

* P25Rflector-Dasboard : https://github.com/ShaYmez/P25Reflector-Dashboard

* P252XLX : https://github.com/yl3im/p25-to-xlx

* P252DMR : https://www.lucifernet.com/2020/01/08/build-a-p25-dmr-cross-mode-bridge/

* pYSFReflector : https://github.com/iu5jae/pYSFReflector3

* YSF2DMR : https://github.com/juribeparada/MMDVM_CM

* Dvswitch : https://dvswitch.groups.io/g/main?

* Dvswitch Dashboard mod by DS5QDR : https://ds5qdr-dv.tistory.com/m/450

* FreeDMR : https://gitlab.hacknix.net/hacknix/FreeDMR/-/wikis/Installing-using-Docker-(recommended!)

* FDMR-Monitor (OA4DOA) : https://github.com/yuvelq/FDMR-Monitor/tree/Self_Service

* FDMR-Monitor2 (CS8ABG) : https://github.com/CS8ABG/FDMR-Monitor/tree/Self_Service

* NoIP : https://www.noip.com/

* GoTTY : https://github.com/yudai/gotty/

* HBLink3 : https://github.com/HBLink-org/hblink3

* HBmonitor (For HBLink) : https://github.com/sp2ong/HBmonitor

* HBmonitor2 (For HBLink) : https://github.com/sp2ong/HBMonv2

* HBJson (For HBLnk) : https://github.com/Avrahqedivra/HBJson

* Balena WiFi Connect : https://github.com/balena-os/wifi-connect 

#

Exitos en sus proyectos con raspberry 

HP3ICC

Esteban Mackay Q.

73.

#
