#!/bin/bash
sudo cat > /bin/menu-update0 <<- "EOFUP0"
#!/bin/bash
while : ; do
choix=$(whiptail --title "Raspbian Proyect HP3ICC Menu Update" --menu "Nota Importante: solo actualice aplicaciones que esten en uso, al finalizar la actualizacion la aplicacion se reiniciara, tenga pleno dominio de las configuraciones de cada aplicacion, antes de actualizar." 17 50 5 \
1 " UPDATE DASHBOARD DVSwitch DS5QDR " \
2 " UPDATE Host Files DVSwitch " \
3 " ADVANCE UPGRADE & INSTALL " \
4 " Menu Principal " 3>&1 1>&2 2>&3)
exitstatus=$?
if [ $exitstatus = 0 ]; then
    echo "Your chosen option:" $choix
else
    echo "You chose cancel."
    break
fi
case $choix in
1)
    sudo bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/install/update-dash-dvs.sh)"
    sudo bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/menu/menu-dvs)"
    ;;
2)
    clear
    echo "Update Host files - please wait"
    sudo /usr/local/sbin/DVSM_Update.sh

    # Lista de servicios a reiniciar
    servicios=("mmdvm_bridge.service" "nxdngateway.service" "p25gateway.service" "ysfgateway.service" "ircddbgatewayd.service")

    # Recorre la lista de servicios y verifica si están activos
    for servicio in "${servicios[@]}"; do
        if systemctl status "$servicio" | grep "Active: active" >/dev/null 2>&1; then
            echo "Reiniciando el servicio $servicio"
            sudo systemctl stop "$servicio"
            sudo systemctl start "$servicio"
        fi
    done
    ;;
3)
    menu-update2
    ;;
4)
    break
    ;;
esac
done
exit 0

EOFUP0
sudo chmod +x /bin/menu-update0
####
sudo cat > /bin/menu-update2 <<- "EOFY2"
#!/bin/bash
while : ; do
choix=$(whiptail --title "Raspbian Proyect HP3ICC Menu Avance Upgrade" --menu "Nota Importante: solo actualice aplicaciones que esten en uso, al finalizar la actualizacion la aplicacion se reiniciara, tenga pleno dominio de las configuraciones de cada aplicacion, antes de actualizar.
" 21 50 8 \
1 " install DVSMU " \
2 " reinstall raspi-config " \
3 " Fix service Dvswitch " \
4 " Menu Principal " 3>&1 1>&2 2>&3)
exitstatus=$?
if [ $exitstatus = 0 ]; then
    echo "Your chosen option:" $choix
else
    echo "You chose cancel."
    break
fi
case $choix in
1)
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/DVSMU/-/raw/main/emq-dvsmu.sh)" ;;
2)
if [ "$(cat /proc/cpuinfo | grep 'Raspberry')" != "" ]; then
apt-get update
sudo apt-get install raspi-config -y
else
  whiptail --title "emq-TE1" --msgbox "Solo para Raspberry / Only for Raspberry" 0 50
	exit 0
fi;;
3)
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/service/service.sh)" &&
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/menu/menu-dvs)" ;;
4)
break
    ;;
esac
done
exit 0
EOFY2
sudo chmod +x /bin/menu-update2
cd /
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/menu/data-mmdvm.sh)"
#
##########################################  End Update Files   ##############################################################
menu-update0
