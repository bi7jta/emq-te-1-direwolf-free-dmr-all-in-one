#!/bin/bash
# Verificar si el usuario tiene permisos de root
if [[ $EUID -ne 0 ]]; then
    echo "Este script debe ejecutarse como usuario ROOT"
    exit 1
fi
# Actualizar la lista de paquetes una vez al principio
 apt-get update && apt-get upgrade -y

bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/install/wifi-connect.sh)" 

# Lista de aplicaciones para verificar e instalar
apps=("sudo" "curl" "git" "make" "build-essential" "libusb-1.0-0-dev" "python" "python3"  "python3-full" "python3-pip" "chkconfig" "git-core" "libi2c-dev" "i2c-tools" "lm-sensors" "python3-websockets" "python3-gpiozero" "python3-psutil" "python3-serial" "wget" "sudo" "python3-dev" "python3-venv" "libffi-dev" "libssl-dev" "cargo" "pkg-config" "sed" "default-libmysqlclient-dev" "libmysqlclient-dev" "build-essential" "zip" "unzip" "python3-distutils" "python3-twisted" "python3-bitarray" "rrdtool" "openssl" "wavemon" "gcc" "g++" "cmake" "libasound2-dev" "libudev-dev" "gpsd" "libgps-dev" "gpsd-clients" "gpsd-tools" "chrony" "libsamplerate0-dev")

# Función para verificar e instalar una aplicación
check_and_install() {
    app=$1
    if ! dpkg -s $app 2>/dev/null | grep -q "Status: install ok installed"; then
        echo "$app no está instalado. Instalando..."
        sudo apt-get install -y $app
        echo "$app instalado correctamente."
    else
        echo "$app ya está instalado."
    fi
}

# Verificar e instalar cada aplicación
for app in "${apps[@]}"; do
    check_and_install $app
done

# Verificar y actualizar python3-venv si no está instalado
if ! dpkg -s python3-venv >/dev/null 2>&1; then
    echo "python3-venv no está instalado. Instalando..."
    apt-get install python3-venv -y
    echo "python3-venv instalado correctamente."
fi

# Crear y activar un entorno virtual
cd /opt/
python3 -m venv myenv
source myenv/bin/activate

# Instalar pip en el entorno virtual
if [ -f "/opt/get-pip.py" ]
then
   rm /opt/get-pip.*
fi
wget https://bootstrap.pypa.io/pip/get-pip.py
python3 get-pip.py
rm get-pip.*

# Instalar paquetes en el entorno virtual
apt-get install -y libssl-dev

python3 -m pip install pip setuptools  
python3 -m pip install cryptography Twisted bitstring MarkupSafe bitarray configparser aprslib attrs wheel service_identity pyOpenSSL mysqlclient tinydb ansi2html mysql-connector-python pandas xlsxwriter cursor pynmea2 maidenhead flask folium mysql-connector resettabletimer setproctitle requests libscrc Pyro5 

# Desactivar el entorno virtual
deactivate

#pip install cryptography pyopenssl autobahn Twisted bitstring MarkupSafe bitarray configparser aprslib attrs wheel service_identity pyOpenSSL mysqlclient tinydb ansi2html mysql-connector-python pandas xlsxwriter cursor pynmea2 maidenhead flask folium mysql-connector resettabletimer setproctitle requests libscrc Pyro5

cd /opt
# Instalar Rust y configurar versión
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y
source $HOME/.cargo/env

rustup install 1.72.0
rustup default 1.72.0

/usr/bin/python3 -m pip install --upgrade pyOpenSSL 
/usr/bin/python3 -m pip install --upgrade autobahn 
/usr/bin/python3 -m pip install --upgrade jinja2 
/usr/bin/python3 -m pip install --upgrade dmr-utils3 
/usr/bin/python3 -m pip install --upgrade ansi2html 
/usr/bin/python3 -m pip install --upgrade aprslib 
/usr/bin/python3 -m pip install --upgrade tinydb 
/usr/bin/python3 -m pip install --upgrade mysqlclient 
/usr/bin/python3 -m pip install --upgrade setproctitle 
/usr/bin/python3 -m pip install --upgrade pynmea2 
/usr/bin/python3 -m pip install --upgrade maidenhead 
/usr/bin/python3 -m pip install --upgrade Twisted 
/usr/bin/python3 -m pip install --upgrade spyne 
/usr/bin/python3 -m pip install --upgrade Pyro5 

echo "Instalación completa."

####################
sudo timedatectl set-timezone America/Panama
####################################################################################
#                                swap raspberry
####################################################################################
if [ "$(cat /proc/cpuinfo | grep 'Raspberry')" != "" ]; then
sudo systemctl stop dphys-swapfile.service
sudo systemctl disable dphys-swapfile.service
sudo update-rc.d dphys-swapfile remove
sudo chmod -x /etc/init.d/dphys-swapfile
sudo dphys-swapfile swapoff
sudo swapoff -a
sudo rm /var/swap
sudo dphys-swapfile uninstall
sudo sed -i 's/CONF_SWAPSIZE=.*/CONF_SWAPSIZE=0/' /etc/dphys-swapfile

fi

######################################################################################################################
#                                                           Cronedit
######################################################################################################################
cat > /usr/local/bin/cronedit.sh <<- "EOF"
cronjob_editor () {
# usage: cronjob_editor '<interval>' '<command>' <add|remove>

if [[ -z "$1" ]] ;then printf " no interval specified\n" ;fi
if [[ -z "$2" ]] ;then printf " no command specified\n" ;fi
if [[ -z "$3" ]] ;then printf " no action specified\n" ;fi

if [[ "$3" == add ]] ;then
    # add cronjob, no duplication:
    ( sudo crontab -l | grep -v -F -w "$2" ; echo "$1 $2" ) | sudo crontab -
elif [[ "$3" == remove ]] ;then
    # remove cronjob:
    ( sudo crontab -l | grep -v -F -w "$2" ) | sudo crontab -
fi
}
cronjob_editor "$1" "$2" "$3"


EOF
sudo chmod +x /usr/local/bin/cronedit.sh
####################
echo iniciando instalacion
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/install/rm.sh)" &&
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/install/dvswitch.sh)" &&
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/install/direwolf.sh)" &&
#bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/install/pymultimonng.sh)"
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/install/ysfreflector.sh)" &&
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/install/mmdvm.sh)" &&
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/install/ysfgateway2.sh)" &&
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/install/gotty.sh)" &&
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/install/noip.sh)" &&
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/python-aprs-beacon/-/raw/main/install.sh)" &&
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/install/rebooters.sh)" &&
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/install/ysf2dmr.sh)" &&
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/install/dmrgw.sh)" &&
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/install/f2b+ufw.sh)" &&
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/python-aprs-wx/-/raw/main/install.sh)" &&
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/install/ysf2ysf.sh)" &&
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/easy-hbl/-/raw/main/hbl.sh)" &&
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/easy-hbl/-/raw/main/hbljson.sh)" &&
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/install/fdmr.sh)" &&
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/fdmr/-/raw/main/fdmr-bridge.sh)" &&
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/install/nxdn2dmr.sh)" &&
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/install/p25.sh)" &&
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/install/p25cross.sh)" &&
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/menu/data-mmdvm.sh)"
echo install menu
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/menu/menu-aprs)" &&
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/python-aprs-beacon/-/raw/main/menu.sh)" &&
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/menu/menu-dvs)" &&
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/menu/menu-dw)" &&
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/menu/menu-fdmr)" &&
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/menu/menu-fdmr-bridge)" &&
#bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/menu/menu-mm-rtl)"
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/menu/menu-mmdvm)" &&
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/menu/menu-noip)" &&
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/menu/menu-reboot)" &&
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/menu/menu-web)" &&
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/menu/menu-ysf)" &&
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/menu/menu-ysf2dmr)" &&
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/menu/menu-ufw)" &&
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/menu/menu-ysf2ysf)" &&
#bash -c "$(curl -fsSL https://gitlab.com/hp3icc/easy-hbl/-/raw/main/menu-hbl.sh)"
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/menu/menu-nxdn2dmr)" &&
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/menu/menu-p25r)" &&
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/menu/menu-wifi-manager)" &&
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/menu/menu-ip)"

############################

bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/install/raspberry.sh)"

# pip install --no-cache-dir --upgrade cryptography pyopenssl autobahn Twisted dmr_utils3 bitstring jinja2 MarkupSafe bitarray configparser aprslib attrs wheel service_identity pyOpenSSL mysqlclient tinydb ansi2html mysql-connector-python pandas xlsxwriter cursor pynmea2 maidenhead flask folium mysql-connector resettabletimer setproctitle requests libscrc Pyro5

(crontab -l; echo "* */1 * * * sync ; echo 3 > /proc/sys/vm/drop_caches >/dev/null 2>&1")|awk '!x[$0]++'|crontab -
sudo systemctl enable gotty.service

#############################################################################################################################
cat > /tmp/completado.sh <<- "EOF"
#!/bin/bash
while : ; do
choix=$(whiptail --title "Raspbian Proyect HP3ICC Esteban Mackay 73." --menu " Precione enter (return o intro) para finalizar la instalacion y reiniciar" 11 85 3 \
1 " Iniciar Reinicio de equipo " 3>&1 1>&2 2>&3)
exitstatus=$?
#on recupere ce choix
#exitstatus=$?
if [ $exitstatus = 0 ]; then
    echo "Your chosen option:" $choix
else
    echo "You chose cancel."; break;
fi
# case : action en fonction du choix
case $choix in
1)
sudo reboot
;;
esac
done
exit 0
EOF
###
##############################
cat > /opt/emq-ver <<- "EOF"
EMQ-VER:  23.10.28
EOF
##############################
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/menu/menu)"
sudo chmod +x /tmp/completado.sh
history -c && history -w
sh /tmp/completado.sh

 
